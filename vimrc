set nocompatible              " be iMproved, required
syntax enable
filetype off                  " required
set number
" automatyczne odświeżanie pliku, gdy został zmieniony w konsoli
set autoread

" kopiowanie ze schowkiem systemowym
set clipboard=unnamedplus

" localny nvimrc
set exrc
set secure

" dla parcela
set backupcopy=yes

" normalny backspace
set backspace=indent,eol,start

" insert mode chudy kursor
au InsertEnter * silent execute "!echo -en \<esc>[5 q"
au InsertLeave * silent execute "!echo -en \<esc>[2 q"

" nie dodawaj znaku \n przy długich wierszach
set textwidth=0 wrapmargin=0

" inny leader
let mapleader = ","

" nie zwijaj długich wierszy
set nowrap

" wyłącznie Ex mode
map Q <Nop>

" omijanie Esc
" inoremap <C-d> <Esc>
" inoremap ;; <Esc>

" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

" sprawdzanie pisowni
" z= - sugestie
" zg - dodaj do słownika
" ]s,[s - skok do błędu
" zw - wypisanie słowa ze słownika (można też manualnie z pliku wykasować)
" set spell spelllang=pl,en_us
" set nospell

" zmiana koloru podkreślenia błędu
" highlight clear SpellBad

" brak słowa w słowniku
" highlight SpellBad ctermfg=009 ctermbg=011 guifg=#ff0000 guibg=#ffff00

" wyraz po kropce pisany małą literą
" highlight SpellCap ctermfg=099 ctermbg=011 guifg=#00ff00 guibg=#ffff00
" highlight SpellRare ctermfg=089 ctermbg=011 guifg=#00ff00 guibg=#ffff00

" auto indent
set autoindent

" ignorowanie wielkości liter; jednak gdy wzorzec ma
" choć jedną dużą literę, wyłącza opcję 'ignorecase'
set ignorecase smartcase 

" zaznaczenie <tekstu> vi<
set matchpairs+=<:>

" szybkie szukanie
set incsearch

" wchodzenie do trybu edycji
nmap <Space> i

" obsługa myszki
set mouse=a

" tabs
set noet ci pi ts=2 sts=2 sw=2
set expandtab

" małe wcięcia dla plików html
autocmd FileType html,clojure setlocal shiftwidth=2 tabstop=2

" scrolling
set scrolloff=5

" przełączanie buforów
map <Leader>n :bp <CR>
map <Leader>m :bn <CR>

" zapisywanie ctrl+s
noremap <silent> <C-S>          :update<CR><Esc>
vnoremap <silent> <C-S>         <C-C>:update<CR><Esc>
inoremap <silent> <C-S>         <C-O>:update<CR><Esc>

" wygodna obsługa schowka systemowego
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" mała indentacja dla różnych typów plików
autocmd Filetype python,elm setlocal sts=0 sw=4 ts=4


" szybszy update, dobre dla git guttera
set updatetime=1000

" filetype hbs, ejs do html
au BufRead,BufNewFile *.ejs,*.hbs setfiletype html

" podświetlanie znalezionych znaków
set hlsearch

" automatyczne dopasowywanie rozmiaru quickfix'a
" au FileType qf call AdjustWindowHeight(3, 10)
" function! AdjustWindowHeight(minheight, maxheight)
"   exe max([min([line("$"), a:maxheight]), a:minheight]) . "wincmd _"
" endfunction

" 
" USTAWIENIA PLUGINÓW (aż do samego końca)
"

" jsx
let g:jsx_ext_required = 0 

" Solarized
set background=light
colorscheme solarized
" let g:solarized_termcolors=256

" Fugitive
" Kolory dla diffa.
" Można podawać z zakresu 000-255 lub 0-15 (profil terminala gnoma).
"
" !!! Zakomentowałem, bo chyba i tak są nadpisane przez solarized !!!
" Jeśli są nadpisane, to należy je usunąć, bo te kolory były potrzebne
" tylko w poprzedniej palecie barw terminala.
"
" hi clear DiffAdd
" hi DiffAdd ctermfg=2
"
" hi clear DiffDelete
" hi DiffDelete ctermfg=052
"
" hi clear DiffChange
" hi DiffChange ctermfg=220
"
" hi clear DiffText
" hi DiffText ctermfg=160
"
" hi clear SignColumn
" hi SignColumn ctermfg=255
"
" hi clear Folded
" hi Folded ctermfg=255
"
" hi clear FoldColumn
" hi FoldColumn ctermfg=255
"
" " nie wiem do czego ta grupa służy, ale źle wygląda...
" hi ColorColumn ctermfg=0

" CtrlP
" zwiększenie prędkości wyszukiwania, ale wymaga instalacji
" silversearcher ag
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
" indeksowanie wszystkich plików w przeglądanym katalogu, bez limitu
let g:ctrlp_max_files=0
map <C-p> :CtrlPCurWD<CR>
let g:ctrlp_map = '<F12>'

set completeopt-=preview
" automatyczne usuwanie okienka z docstringami
" autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" airline
set t_Co=256 " 256 kolorów w terminalu
set laststatus=2
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"

" jedi
" wyłączenie wybrania pierwszego elementu po wpisaniu kropki
let g:neocomplcache_enable_at_startup = 1
if !exists('g:neocomplcache_omni_functions')
    let g:neocomplcache_omni_functions = {}
endif
let g:neocomplcache_omni_functions['python'] = 'jedi#completions'
let g:jedi#popup_on_dot = 0

" localvimrc
let g:localvimrc_ask = 0

" ale
" let g:ale_linters = {
" \   'javascript': ['standard'],
" \   'json': ['jsonlint'],
" \}
"
" " dla flow
" " \   'javascript': ['flow', 'standard'],
" " let g:ale_javascript_standard_options = "--parser babel-eslint --plugin flowtype"
"
" let g:ale_fixers = {
" \   'elm': ['elm-format']
" \}
" " \   'javascript': ['eslint']
" let g:ale_fix_on_save = 1

" " tymczasowo wyłączono z powodu zmiany na Coc
" " skakanie do błędów
" nmap <silent> <C-Up> <Plug>(ale_previous_wrap)
" nmap <silent> <C-Down> <Plug>(ale_next_wrap)
"
" " błędy na belce statusu
" function! LinterStatus() abort
"   let l:counts = ale#statusline#Count(bufnr(''))
"
"   let l:all_errors = l:counts.error + l:counts.style_error
"   let l:all_non_errors = l:counts.total - l:all_errors
"
"   return l:counts.total == 0 ? 'OK' : printf(
"         \   '%dW %dE',
"         \   all_non_errors,
"         \   all_errors
"         \)
" endfunction
"
" set statusline=%{LinterStatus()}
" " let g:ale_set_quickfix = 1
" " let g:ale_open_list = 1

" map Nerdtree i Taglist
map <Leader>w :NERDTreeToggle <CR>
map <Leader>\ :NERDTreeFind <CR>
map <Leader>q :TlistToggle <CR>
let Tlist_Show_One_File = 1  " tagi tylko dla aktywnego bufora

" YouCompleteMe
" tymczasowo zastąpione przez coc
" goto
" nnoremap <leader>d :YcmCompleter GoTo<CR>
" " plik wrzucić do katalogu domowego, dodać kropkę na początku
" let g:ycm_global_ycm_extra_conf = '~/'
" let g:ycm_complete_in_comments = 1 " Completion in comments
" let g:ycm_complete_in_strings = 1 " Completion in string
" let g:ycm_seed_identifiers_with_syntax = 1 " Completion for programming language's keyword

" UltiSnips
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:UltiSnipsEditSplit=" vertical"
" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" EnchantedVim - very magic
" włączenie opcji very magic gdzie tylko się da
let g:VeryMagicSubstitute = 1
let g:VeryMagicGlobal = 1
let g:VeryMagicVimGrep = 1
let g:VeryMagicSearchArg = 1
let g:VeryMagicFunction = 1
let g:VeryMagicHelpgrep = 1
let g:VeryMagicRange = 1
let g:VeryMagicEscapeBackslashesInSearchArg = 1  " (:edit +/{pattern}))
let g:SortEditArgs = 1

" włączenie kompatybilności z funkcją incsearch
let g:VeryMagic = 0
nm / /\v
nm ? ?\v

" tcomment
autocmd FileType kivy set commentstring=#%s

" delimitMate
let delimitMate_matchpairs = "(:),[:],{:}"

" closetag
let g:closetag_filenames = "*.html,*.xml,*.htm,*.xhtml,*.ejs"

" CtrlSF
map <leader>ff <Plug>CtrlSFCwordPath
map <leader>fg <Plug>CtrlSFPrompt
map <leader>fd <Plug>CtrlSFVwordPath
let g:ctrlsf_regex_pattern = 1
let g:ctrlsf_auto_focus = {
    \ "at": "start"
    \ }


" gitgutter
map <leader>ggu :GitGutterUndo<CR>
map <leader>ggp :GitGutterPreviewHunk<CR>


" elm
let g:elm_jump_to_error = 0 "0
let g:elm_make_output_file = "elm.js"
let g:elm_make_show_warnings = 1 "0
let g:elm_syntastic_show_warnings = 0
let g:elm_browser_command = ""
let g:elm_detailed_complete = 1 "0
let g:elm_format_autosave = 0 "0
let g:elm_format_fail_silently = 0
let g:elm_setup_keybindings = 0 "1


" CoC


" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=2

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
" set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" navigate diagnostics
nmap <silent> <C-Up> <Plug>(coc-diagnostic-prev)
nmap <silent> <C-Down> <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')
noremap <silent> <leader>f :Format<CR><Esc>

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')


" ////////

" :PlugInstall - instalacja, :PlugClean - deinstalacja, :PlugUpdate - update
call plug#begin('~/.vim/bundle')

Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }
Plug 'vim-scripts/taglist.vim'
Plug 'nvie/vim-flake8', {'for': 'python'}
Plug 'scrooloose/nerdcommenter' " zamiennik dla tcomment
Plug 'tomtom/tcomment_vim'
Plug 'tpope/vim-fugitive'
Plug 'bling/vim-airline'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'SirVer/ultisnips' " 1. razem
Plug 'honza/vim-snippets' " 1. razem
" Plug 'ervandew/supertab' " 1.razem
Plug 'coot/CRDispatcher' " 2. razem
Plug 'coot/EnchantedVim' " 2. razem, dodawanie \v do regexów
" extra obiekty tekstowe, szczególnie przydatne do vim surround,
" np. do otagowania linii kodu, wpisać: ysil<i> (gdzie il - linia bez \n)
Plug 'kana/vim-textobj-user' " 3. razem
Plug 'kana/vim-textobj-line' " 3. razem
Plug 'bps/vim-textobj-python', {'for': 'python'} " 3. razem, af/ac do zaznaczenia funkcji/klasy
Plug 'kien/ctrlp.vim' " ulepszony command-t
Plug 'pangloss/vim-javascript', {'for': ['javascript', 'typescript']} " podświetlanie nawiasów bez pary
Plug 'Raimondi/delimitMate' " automatyczne nawiasy
Plug 'editorconfig/editorconfig-vim'
Plug 'altercation/vim-colors-solarized'
Plug 'leafgarland/typescript-vim', {'for': 'typescript'}
Plug 'airblade/vim-gitgutter'
Plug 'alvan/vim-closetag'
Plug 'danro/rename.vim'
Plug 'ElmCast/elm-vim', {'for': 'elm'}
Plug 'tpope/vim-haml', {'for': 'haml'}
" Plug 'w0rp/ale'
Plug 'dyng/ctrlsf.vim'
" clojure
" Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people', {'for': 'clojure'}
Plug 'mxw/vim-jsx', {'for': 'javascript'}
" Plug 'Quramy/tsuquyomi', {'for': 'typescript'} " laguje
" Plug 'Valloric/YouCompleteMe'
Plug 'embear/vim-localvimrc'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

filetype plugin indent on    " required
